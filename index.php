<?php
  $host = 'localhost';
  $dbname = 'dc2s10';
  $user = 'root';
  $pass = 'root';
  $port = '8889';

  try {
    $dbh = new PDO("mysql:host=$host;port=$port;dbname=$dbname;", $user, $pass);
    // echo '<p>Connected to database</p>';
  } catch(PDOException $e) {
    // echo "<p>Oop, something went wrong.</p>";
    die("DB Error: ". $e->getMessage());
  }

  $stmt = $dbh->query('SELECT * FROM users');

  /*
    PDO:FETCH_ASSOC puts the results in an array where values are mapped to their field names.

      i.e. $users would look like:

        Array (
          [0] => Array (
            [id] => 1
            [first_name] => Chris
            [last_name] => Edwards
          )
          [1] => Array (
            [id] => 2
            [first_name] => Steven
            [last_name] => Jones
          )
          [2] => Array (
            [id] => 3
            [first_name] => Tom
            [last_name] => Hanks
          )
        )
  */

  $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/assets/ico/favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Users</a></li>
            <li><a href="/new_user.php">New user</a></li>
            <li><a href="/new_post.php">New post</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <ol>
            <?php
              /* This is looping through each of the users returned from the statement above,
                referring to each one as "$user" as it does so.

                If we were to <?php print_r($user); ?> we would see all the fields inside each user,
                mapped in an array of [key] => value e.g. [first_name] => Chris

                This can be helpful when debugging.
              */
              foreach($users as $user) {
                echo "<li>" . $user['first_name'] . ' ' . $user['last_name'] . "</li>";
              }
            ?>
          </ol>
        </div>

        <div class="col-md-3">
          <div class="panel panel-default">

          </div>
        </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
  </body>
</html>
