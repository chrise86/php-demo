<?php

  $host = 'localhost';
  $dbname = 'dc2s10';
  $user = 'root';
  $pass = 'root';
  $port = '8889';

  try {
    $dbh = new PDO("mysql:host=$host;port=$port;dbname=$dbname;", $user, $pass);
    // echo '<p>Connected to database</p>';
  } catch(PDOException $e) {
    // echo "<p>Oop, something went wrong.</p>";
    die("DB Error: ". $e->getMessage());
  }

  // Simple, doesnt check for presence of first_name
  // $first_name = $_POST['first_name'];

  // Checks if first_name is present, otherwise set it as blank
  // Shorthand
  // $first_name = isset($_POST['first_name']) ? $_POST['first_name'] : 'missing';

  // Checks if first_name is present, otherwise set it as blank
  // Long
  if (isset($_POST['first_name'])) {
    $first_name = $_POST['first_name'];
  } else {
    $first_name = 'missing';
  }

  if (isset($_POST['last_name'])) {
    $last_name = $_POST['last_name'];
  } else {
    $last_name = 'missing';
  }

  $sql = "INSERT INTO users ( first_name, last_name ) VALUES ( :first_name, :last_name )";

  $query = $dbh->prepare( $sql );

  $query->execute( array( ':first_name'=>$first_name, ':last_name'=>$last_name ) );


?>